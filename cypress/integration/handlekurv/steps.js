import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
  cy.visit("http://localhost:8080");
});

When(/^jeg legger inn varer og kvanta$/, () => {
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();

  cy.get("#product").select("Smørbukk");
  cy.get("#quantity").clear().type("5");
  cy.get("#saveItem").click();

  cy.get("#product").select("Stratos");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();

  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
  // TODO: Verifiser innholdet i lista med should()

  cy.get("#list").should("contain", "Hubba bubba");
  cy.get("#list").should("contain", "Smørbukk");
  cy.get("#list").should("contain", "Stratos");
  cy.get("#list").should("contain", "Hobby");
});

And(/^den skal ha riktig totalpris$/, function () {
  cy.get("#price").should("have.text", "33");
});

Given(/^at jeg har åpnet nettkiosken$/, () => {
  cy.visit("http://localhost:8080");
});

And(/^lagt inn varer og kvanta$/, function () {
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();
});

When(/^jeg sletter varer$/, () => {
  cy.get("#deleteItem").click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, () => {
  cy.get("#list").should("not.contain", "Hubba bubba");
});

Given(/^at jeg har åpnet nettkiosken$/, () => {
  cy.visit("http://localhost:8080");
});

And(/^lagt inn varer og kvanta$/, function () {
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();
});

When(/^jeg oppdaterer kvanta for en vare$/, () => {
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
  cy.get("#list").should("contain", "2 Hubba bubba");
});

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit("http://localhost:8080");
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();
});

And(/^trykket på Gå til betaling$/, function () {
  cy.get("#goToPayment").click();
});

When(
  /^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/,
  () => {
    cy.get("#fullName").clear().type("Bob Bobbs");
    cy.get("#address").clear().type("Bobgate 2");
    cy.get("#postCode").clear().type("808");
    cy.get("#city").clear().type("Bobo City");
    cy.get("#creditCardNo").clear().type("8080808080808080");
  }
);

And(/^trykker på Fullfør kjøp$/, function () {
  //KOMMENTAR: Usikker her ...
  cy.get('button[type="submit"]').click();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
  cy.contains("Din ordre er registrert.");
});

Given(/^ Gitt at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit("http://localhost:8080");
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();
});

And(/^trykket på Gå til betaling$/, function () {
  cy.get("#goToPayment").click();
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
  cy.get("#fullName").clear().blur();
  cy.get("#address").clear().blur();
  cy.get("#postCode").clear().blur();
  cy.get("#city").clear().blur();
  cy.get("#creditCardNo").clear().blur();

  //KOMMENTAR: Usikker her ...
  cy.get('button[type="submit"]').click();
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
  cy.get("#fullNameError").should("have.text", "Feltet må ha en verdi");
  cy.get("#addressError").should("have.text", "Feltet må ha en verdi");
  cy.get("#postCodeError").should("have.text", "Feltet må ha en verdi");
  cy.get("#cityError").should("have.text", "Feltet må ha en verdi");
  cy.get("#creditCardNoError").should(
    "have.text",
    "Kredittkortnummeret må bestå av 16 siffer"
  );
});
