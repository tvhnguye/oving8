# language: no
Egenskap: Handlekurv

  Som kunde av nettkiosken
  Ønsker jeg å legge varer i handlekurven
  Slik at jeg kan få godis i posten

  Scenario: Legge til varer i handlekurven
    Gitt at jeg har åpnet nettkiosken
    Når jeg legger inn varer og kvanta
    Så skal handlekurven inneholde det jeg har lagt inn
    Og den skal ha riktig totalpris
    
  Scenario: Gitt at jeg har åpnet nettkiosken 
    Og lagt inn varer og kvanta 
    Når jeg sletter varer 
    Så skal ikke handlekurven inneholde det jeg har slettet

  Scenario: Gitt at jeg har åpnet nettkiosken 
    Og lagt inn varer og kvanta 
    Når jeg oppdaterer kvanta for en vare 
    Så skal handlekurven inneholde riktig kvanta for varen


  Som kunde av nettkiosken 
  Ønsker jeg å kunne legge inn betalingsinformasjon 
  Slik at jeg kan få godis i posten

  Scenario: Gitt at jeg har lagt inn varer i handlekurven 
    Og trykket på Gå til betaling 
    Når jeg legger inn navn, adresse, postnummer, poststed og kortnummer 
    Og trykker på Fullfør kjøp 
    Så skal jeg få beskjed om at kjøpet er registrert

  Scenario: Gitt at jeg har lagt inn varer i handlekurven 
    Og trykket på Gå til betaling 
    Når jeg legger inn ugyldige verdier i feltene 
    Så skal jeg få feilmeldinger for disse